/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2;

import laboratorio2.trigonometria.*;

/**
 *
 * @author anonimo
 */
public class Laboratorio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Punto p1 = new Punto(1,10);
        Punto p2 = new Punto(3,20);
                       
        double dist;
        dist= p1.calcularDistancia(p2);
        
        System.out.println("\ndistancia entre  puntos: " + dist);
        
        
        Circulo c = new Circulo(p1,30);
        double perim ;
        perim = c.calcularPerimetro();
        
        System.out.println("\nperimetro de circ: " + perim);
        
        
        
        
    }
    
}
